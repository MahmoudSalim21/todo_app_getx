import 'package:flutter/material.dart';
import 'package:todo_app_getx/services/theme_services.dart';
import 'package:todo_app_getx/ui/pages/home_page.dart';
import 'package:todo_app_getx/ui/pages/notification_screen.dart';
import 'package:get/get.dart';
import 'package:todo_app_getx/ui/theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: Themes.light,
      darkTheme: Themes.dark,
      themeMode: ThemeServices().theme,
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: HomePage(

      )
    );
  }
}
